package com.example.tennisapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.tennisapp.data.SplashResponse;
import com.example.tennisapp.util.MyServerClient;
import com.example.tennisapp.util.ServerClient;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebViewActivity extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(visibility -> fullscreen());
        fullscreen();
        runAd();
    }

    public void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

    }

    private void runAd() {
        ServerClient client = MyServerClient.createRxService(ServerClient.class);
        Call<SplashResponse> call = client.getSplash(Locale.getDefault().getLanguage());
        call.enqueue(new Callback<SplashResponse>() {
            @Override
            public void onResponse(@NonNull Call<SplashResponse> call, @NonNull Response<SplashResponse> response) {
                if (response.isSuccessful()) {
                    String s = response.body().url;
                    if(s.equals("no")) {
                        Toast.makeText(getApplicationContext(),"No", Toast.LENGTH_SHORT).show();
                        switchToMain();
                    }
                    else {
                        WebView webView = findViewById(R.id.splash_view);
                        webView.setWebViewClient(new WebViewClient());
                        WebSettings webSettings = webView.getSettings();
                        webSettings.setJavaScriptEnabled(true);
                        webSettings.setAllowContentAccess(true);
                        webView.loadUrl("https://" + s);
                        fullscreen();
                    }
                } else {
                    Toast.makeText(getApplicationContext(),"Server error 2", Toast.LENGTH_SHORT).show();
                    switchToMain();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SplashResponse> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(),"Server error 3", Toast.LENGTH_SHORT).show();
                switchToMain();
            }
        });
    }

    private void switchToMain() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_NO_ANIMATION);
        getApplicationContext().startActivity(i);
        finish();
    }

}
package com.example.tennisapp.util;

import com.example.tennisapp.data.SplashResponse;
import com.example.tennisapp.data.TextElement;
import com.example.tennisapp.data.Tip;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ServerClient {

    @GET("TennisApp/{file}")
    Observable<List<TextElement>> getText(@Path("file") String file);

    @GET("TennisApp/{file}")
    Observable<List<Tip>> getTips(@Path("file") String file);

    @FormUrlEncoded
    @POST("TennisApp/splash.php")
    Call<SplashResponse> getSplash(@Field("locale") String locale);

}

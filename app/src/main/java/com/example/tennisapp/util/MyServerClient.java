package com.example.tennisapp.util;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyServerClient {
    private static final String BASE_URL = "http://159.69.89.54/";

    private static Retrofit rxRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .build();

    public static <S> S createRxService(Class<S> serviceClass) {
        return rxRetrofit.create(serviceClass);
    }
}

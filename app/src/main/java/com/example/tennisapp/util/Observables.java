package com.example.tennisapp.util;

import com.example.tennisapp.data.TextElement;
import com.example.tennisapp.data.Tip;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class Observables {
    private static ServerClient serverClient = MyServerClient.createRxService(ServerClient.class);

    private static Observable<List<TextElement>> rulesObs = serverClient.getText("rules.json")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .replay().autoConnect();

    private static Observable<List<TextElement>> rulesHis = serverClient.getText("history.json")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .replay().autoConnect();

    private static Observable<List<TextElement>> rulesTac = serverClient.getText("tactics.json")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .replay().autoConnect();

    private static Observable<List<Tip>> tips = serverClient.getTips("tips/hint_map.json")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .replay().autoConnect();

    public static void subscribeToRules(Observer<List<TextElement>> observer) {
        rulesObs.subscribe(observer);
    }

    public static void subscribeToHistory(Observer<List<TextElement>> observer) {
        rulesHis.subscribe(observer);
    }

    public static void subscribeToTactic(Observer<List<TextElement>> observer) {
        rulesTac.subscribe(observer);
    }

    public static void subscribeToTips(Observer<List<Tip>> observer) {
        tips.subscribe(observer);
    }

}

package com.example.tennisapp.util;

import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tennisapp.R;
import com.example.tennisapp.data.TextElement;
import com.squareup.picasso.Picasso;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;

public class TextFormat {
    private static final int HEADER = 0;
    private static final int TEXT = 1;
    private static final int IMG = 2;

    public static void fillLayout(LinearLayout layout, List<TextElement> text) {
        Observable<TextElement> observable = Observable.fromIterable(text);

        observable.subscribe(e -> addToLayout(layout, e));
    }

    public static void addToLayout(LinearLayout layout, TextElement text) {
        if(text != null && layout != null) {
            switch (getType(text)) {
                case HEADER:
                    TextView header = new TextView(layout.getContext());
                    header.setText(text.getHeader());
                    header.setGravity(Gravity.CENTER);
                    header.setTextSize(24);
                    header.setTextColor(layout.getResources().getColor(R.color.black));
                    header.setBackgroundColor(layout.getResources().getColor(R.color.white));
                    layout.addView(header, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    break;
                case IMG:
                    ImageView image = new ImageView(layout.getContext());
                    layout.addView(image, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    Picasso.get().load(text.getImg()).into(image);
                    break;
                case TEXT:
                    TextView textView = new TextView(layout.getContext());
                    textView.setText(text.getText());
                    textView.setTextSize(14);
                    textView.setTextColor(layout.getResources().getColor(R.color.black));
                    textView.setBackgroundColor(layout.getResources().getColor(R.color.white));
                    layout.addView(textView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    break;
            }
        }
    }

    private static int getType(TextElement textElement) {
        if(!textElement.getHeader().equals("")) return HEADER;
        else if(!textElement.getImg().equals("")) return IMG;
        else  return TEXT;
    }
}

package com.example.tennisapp.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tennisapp.R;
import com.example.tennisapp.data.Tip;

import java.util.List;

public class TipAdapter extends RecyclerView.Adapter<TipAdapter.ViewHolder> {
    private List<Tip> tips;
    private cLInterface clickListener;

    public TipAdapter(List<Tip> tips) { this.tips = tips; }

    @NonNull
    @Override
    public TipAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tip_recycler, parent, false);
        return new TipAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TipAdapter.ViewHolder holder, int position) {
        holder.header.setText(tips.get(position).getName());
    }

    @Override
    public int getItemCount() {
        if(tips != null) return tips.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView header;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.item_tip_header);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onTipClick(tips.get(getAdapterPosition()).getPath());
        }
    }

    public void setListener(TipAdapter.cLInterface clickListener) {
        this.clickListener = clickListener;
    }

    public interface cLInterface {
        void onTipClick(String path);
    }
}

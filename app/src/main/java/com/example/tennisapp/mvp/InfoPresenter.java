package com.example.tennisapp.mvp;

import com.example.tennisapp.data.TextElement;
import com.example.tennisapp.util.MyServerClient;
import com.example.tennisapp.util.ServerClient;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class InfoPresenter extends MvpPresenter<InfoPresenterViewInterface> {
    Disposable d;

    public void initialise(String path) {
        ServerClient serverClient = MyServerClient.createRxService(ServerClient.class);
        Observable<List<TextElement>> text = serverClient.getText(path)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        text.subscribe(new Observer<List<TextElement>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<TextElement> textElements) {
                getViewState().loadText(textElements);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        d.dispose();
    }
}

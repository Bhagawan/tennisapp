package com.example.tennisapp.mvp;

import com.example.tennisapp.data.TextElement;
import com.example.tennisapp.util.Observables;

import java.util.List;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class RulesPresenter extends MvpPresenter<RulerPresenterViewInterface> {

    @Override
    protected void onFirstViewAttach() {
        Observables.subscribeToRules(new Observer<List<TextElement>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull List<TextElement> textElements) {
                getViewState().loadText(textElements);
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}

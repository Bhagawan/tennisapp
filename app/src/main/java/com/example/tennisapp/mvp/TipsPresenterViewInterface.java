package com.example.tennisapp.mvp;

import com.example.tennisapp.data.Tip;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.SingleState;

public interface TipsPresenterViewInterface extends MvpView {
    @SingleState
    void loadTips(List<Tip> tips);
}

package com.example.tennisapp.mvp;

import com.example.tennisapp.data.TextElement;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.SingleState;

public interface HistoryPresenterViewInterface extends MvpView {

    @SingleState
    void loadText(List<TextElement> text);
}

package com.example.tennisapp;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

import com.onesignal.OneSignal;

public class TennisApp extends Application {
    private static final String ONESIGNAL_APP_ID = "be5792b2-35a1-45f5-98de-e16ee7aef076";

    @Override
    public void onCreate() {
        super.onCreate();

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
    }
}

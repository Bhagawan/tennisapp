package com.example.tennisapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.tennisapp.R;

public class MenuFragment extends Fragment {
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_menu, container, false);

        ImageButton rulesButton = mView.findViewById(R.id.btn_menu_rules);
        rulesButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.rulesFragment));
        ImageButton historyButton = mView.findViewById(R.id.btn_menu_history);
        historyButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.historyFragment));
        ImageButton tacticsButton = mView.findViewById(R.id.btn_menu_tactics);
        tacticsButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.tacticsFragment));
        ImageButton tipsButton = mView.findViewById(R.id.btn_menu_tips);
        tipsButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.tipsFragment));
        ImageButton bigHelm = mView.findViewById(R.id.btn_menu_tournament);
        bigHelm.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("path", "big_helm_history.json");
            Navigation.findNavController(v).navigate(R.id.infoFragment, bundle);
        });
        return mView;
    }
}
package com.example.tennisapp.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.tennisapp.R;
import com.example.tennisapp.data.Tip;
import com.example.tennisapp.mvp.TipsPresenter;
import com.example.tennisapp.mvp.TipsPresenterViewInterface;
import com.example.tennisapp.util.TipAdapter;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;


public class TipsFragment extends MvpAppCompatFragment implements TipsPresenterViewInterface {
    private View mView;

    @InjectPresenter
    TipsPresenter mPresenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Navigation.findNavController(requireActivity(),R.id.fragmentContainerView).navigate(R.id.menuFragment);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_tips, container, false);

        ImageButton backButton = mView.findViewById(R.id.btn_tips_back);
        backButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.menuFragment));

        return mView;
    }

    @Override
    public void loadTips(List<Tip> tips) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_tips);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        TipAdapter adapter = new TipAdapter(tips);
        adapter.setListener(path -> {
            Bundle bundle = new Bundle();
            bundle.putString("path", path);
            Navigation.findNavController(mView).navigate(R.id.infoFragment, bundle);
        });
        recyclerView.setAdapter(adapter);
    }
}
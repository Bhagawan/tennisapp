package com.example.tennisapp.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.example.tennisapp.R;
import com.example.tennisapp.data.TextElement;
import com.example.tennisapp.mvp.HistoryPresenter;
import com.example.tennisapp.mvp.HistoryPresenterViewInterface;
import com.example.tennisapp.util.TextFormat;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;


public class HistoryFragment extends MvpAppCompatFragment implements HistoryPresenterViewInterface {
    private View mView;

    @InjectPresenter
    HistoryPresenter mPresenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Navigation.findNavController(requireActivity(),R.id.fragmentContainerView).navigate(R.id.menuFragment);
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_history, container, false);

        ImageButton backButton = mView.findViewById(R.id.btn_history_back);
        backButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.menuFragment));

        return mView;
    }

    @Override
    public void loadText(List<TextElement> text) {
        LinearLayout ll = mView.findViewById(R.id.ll_history);
        TextFormat.fillLayout(ll, text);
    }
}
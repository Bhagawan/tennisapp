package com.example.tennisapp.data;

import com.google.gson.annotations.SerializedName;

public class TextElement {
    @SerializedName("header")
    private String header;
    @SerializedName("text")
    private String text;
    @SerializedName("img")
    private String img;

    public String getHeader() {
        return header;
    }

    public String getText() {
        return text;
    }

    public String getImg() {
        return img;
    }
}

package com.example.tennisapp.data;

import com.google.gson.annotations.SerializedName;

public class SplashResponse {
    @SerializedName("url")
    public String url;
}

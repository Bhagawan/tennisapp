package com.example.tennisapp.data;

import com.google.gson.annotations.SerializedName;

public class Tip {
    @SerializedName("name")
    private String name;
    @SerializedName("file")
    private String path;

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }
}
